import React, { useState }  from 'react';

export const LearningUseState = () => {

  // explicar que contiene el useState [ valor, funcion de actualizacion ];
  // useState es una funcion que pide un valor inicial de cualquier tipo
  // y regresa un arreglo de valores, en su posicion 0 regresa el valor
  // actual de la variable, y en la posicion 1 regresa una funcion
  // para actualizar esta variable

  // 1.- crear el useState sin desestructurar
  // const counter = useState(10);
  // 2.- Explicar la estructuracion del useState y crearlo
  // NOTA --- explicar en una variable nueva
  // const counter = useState(10); //**
  // const [ value, setNewValue ] = counter; //**
  // mostrar la forma ordinaria de desestructurar.
  const [ value, setNewValue ] = useState(0);

  // 1. mostrar como se imprime en consola
  // console.log(value);

  // 3.- Creact una funcion para manipular el useState del click
  const handleClick = ({ target }) => {
    const { action } = target.dataset;
    action === '+' && setNewValue( value + 1);
    action === '-' && setNewValue( value - 1);
  };

  return (
    <>
      <h1>{ value }</h1>
      <button onClick={handleClick} data-action="-"> - 1 </button>
      <button onClick={handleClick} data-action="+"> + 1 </button>
    </>
  );

  // 2.- Refactorizar el con la desestructuracion ordinaria
  // return (
  //   <>
  //     <h1>{ value }</h1>
  //     <button onClick={() => setNewValue( statePrev => statePrev + 1 )}>
  //       +1
  //     </button>
  //   </>
  // );
  // 1. Mostrar el render con arreglos
  // return (
  //   <>
  //     <h1> counter + 1 </h1>
  //     <button onClick={() => {
  //       counter[1]( statePrev => statePrev + 1 );
  //     }}> +1 </button>
  //     { counter[0] }
  //   </>
  // );
};
