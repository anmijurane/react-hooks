// este ejemplo, explica el uso de useEffect es todos sus casos posibles.

import React, { useEffect, useState } from 'react';

export const LearningUseEffect = () => {

  const [ counter, setCounter ] = useState(0);
  const [ counter2, setCounter2 ] = useState(10);
  const [ unMountComponent, setUnMountComponent ] = useState(false);

  //0. Mostrar sintaxis de useEffect

  //1 useEffect sin arreglo de dependencias
  useEffect(() => {
    console.log('Siempre se ejecutará. - ' + 1);
    // este callback se ejecutara siempre que componente se vuelva a renderizar
    // porque no existe una dependencia que prohiba su ejecucion.
  });
  //2 useEffect con arreglo de dependencias vacio
  useEffect(() => {
    console.log('Se ejecutará solo una vez. - ' + 2 )
    // este callback se ejecutara solo la primera vez que se renderice el componente
  }, []);
  //3 useEffect con arreglo de dependencias
  useEffect(() => {
    console.log('Se agrego un numero al contador. - ' + 3);
  }, [counter])
  
  //4 uso del return del useEffect
  useEffect(
    () => {
      // en el return se ejecuta el willUnMount
      return () => {}
    }, []
  )

  return (
    <>
      <h1> primer counter padre { counter } </h1>
      <button onClick={() => setCounter(counter + 1)}> +1 </button>
      <hr />
      <button onClick={() => setUnMountComponent(!unMountComponent) }> toogle mount unMount component </button>
      {/* Crear un toggle para mostrar el desmonte del componente */}
      {unMountComponent && (
        <CounterGeneric
          counterState={[counter, setCounter]}
        />
      )}
      <hr />
      <h1> segundo counter padre { counter2 } </h1>
      <button onClick={() => setCounter2(counter2 + 1)}> +1 </button>
    </>
  )
}

// este componente nos permitira, mostrar el unMount del componente.
// con el return se ejecuta una limpieza del componente o alguna accion.
const CounterGeneric = ({ counterState }) => {

  const [ counter, setCounter ] = counterState;
  const [ counterToLocal, setCounterToLocal ] = useState(0);

  useEffect(() => {
    setCounter( counter + 2 );
    return () => setCounter( 0 );
  }, [counterToLocal]);

  return (
    <>
      <h1> primer counter hijo { counterToLocal }</h1>
      <button onClick={() => setCounterToLocal(counterToLocal + 1 ) } > + 1 = CounterLocal </button>
      <small> counter pasado por el padre al hijo { counter } </small>
    </>
  );
};

