import React, { useEffect, useMemo, useState } from 'react';

export const LearningUseMemo = () => {

  // la memorizacion es una tecnica de ahorro de procesamiento,
  // que evita el recalculo de la funcion/
  // Un ejemplo es que si una funcion siempre sumara 5 + 5 siempre el resultado
  // será 10, por ello no es necesario volver a calcular el resultado, se memoriza
  // y cuando sea necesario se llama.
  useMemo(
    // calback
    () => {
      //process
    },
    [
      // arreglo de dependencias 
    ]
  );

  const [firstNumer, setFirstNumer] = useState(0);
  const [secondNumber, setSecondNumber] = useState(0);
  const [thirdNumber, setthirdNumber] = useState(0);
  const [operation, setOperation] = useState(true);

  const handleOperationChange = ({ target }) => {
    // true = suma | false = resta
    setOperation( target.value === "suma" );
  };

  const makeSum = useMemo(() => {
    console.log(" suma ");
    console.log({firstNumer, secondNumber});
    return firstNumer + secondNumber
    // mostrar que pasa si solo se ejecuta una dependencia;
  }, [ firstNumer ])
  
  // Sin memorizar, se ejecuta a cada rato.
  // const makeSum = () => {
    //   console.log(" suma ");
    //   return firstNumer + secondNumber
    // };
    
  const makeRes = useMemo(() => {
    console.log(" resta ");
    console.log({firstNumer, secondNumber});
    return firstNumer - secondNumber
    // hacer ejercicios de dependencias. 
  }, []);

  // sin memorizar, se ejecuta a cada rato.
  // const makeRes = () => {
    //   console.log(" resta ");
    //   return firstNumer - secondNumber
  // };

  const result = operation ? makeSum : makeRes;


  return (
    <>
      <p>Suma o Resta</p>
      <select name="operation" id="operation" onChange={handleOperationChange}>
        <option value="suma">Suma</option>
        <option value="resta">Resta</option>
      </select>
      <InputGeneric
        label='Numero 1'
        name='number1'
        onChangeInput={ ({ target }) => setFirstNumer( parseFloat( target.value ) ) }
        type='number'
        value={firstNumer}
      />
      <p> + </p>
      <InputGeneric
        label='Numero 2'
        name='number2'
        onChangeInput={ ({ target }) => setSecondNumber( parseFloat( target.value ) ) }
        type='number'
        value={secondNumber}
      />
      <InputGeneric
        label='Numero 3'
        name='number3'
        onChangeInput={ ({ target }) => setthirdNumber( parseFloat( target.value ) ) }
        type='number'
        value={thirdNumber}
      />
      <h2> Resultado: { result } </h2>
    </>
  )
}

const InputGeneric = ({
  name, type, label, value, onChangeInput
}) => {
  return (
    <div className={`input__generic`}>
      <label htmlFor={ name }>{ label }</label>
      <input
        type={type}
        name={name}
        value={value}
        onChange={onChangeInput}
      />
    </div>
  );
};

