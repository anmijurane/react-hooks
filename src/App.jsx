import { LearningUseMemo } from "./components/useMemo";

export const App = () => {
  return (
    <div className="container">
      <LearningUseMemo />
    </div>
  )
}
