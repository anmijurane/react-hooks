// este ejemplo muestra el uso de un formulario mediante un estado
import React, { useState } from 'react';

// crear objeto para renderizar
const formData = {
  name: {
  label: 'Nombre completo',
  name: 'name',
  type: 'text',
    value: ''
  },
  email: {
    label: 'Correo Electronico',
    name: 'email',
    type: 'email',
    value: ''
  },
  numberPhone: {
    label: 'Numero de telefono',
    name: 'numberPhone',
    type: 'phone',
    value: ''
  },
};

export const FormApp = () => {

  const [ form, setForm ] = useState(formData);

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setForm({ ...form, [name]: { ...form[name], value } });
  };

  const handleSubmit = ( e ) => {
    e.preventDefault();
    console.log( form );
  };

  return (
    <form onSubmit={handleSubmit}>
      {Object.keys(form).map( item => {
        const { name, label, type, value } = form[item];
        return (
          <InputGeneric
            key={name}
            name={name}
            label={label}
            type={type}
            onChangeInput={handleChange}
            value={value}
          />
        )
      })}
      <button type="submit"> Enviar </button>
    </form>
  );
};

/*

*/
const InputGeneric = ({
    name, type, label, value, onChangeInput
  }) => {
  return (
    <div className={`input__generic`}>
      <label htmlFor={ name }>{ label }</label>
      <input
        type={type}
        name={name}
        value={value}
        onChange={onChangeInput}
      />
    </div>
  );
};
