import React, { useRef } from 'react'

export const LearningUseRef = () => {

  const inputRef = useRef();

  const handleClick = () => {
    const input = inputRef.current;
    input.focus();
    input.select();
    document.execCommand('copy');
  };

  return (
    <>
      <input
      // este reserva un espacio en memoria, es mas exacto y unico
      // por ello el id puede volverse inestable
        ref={inputRef}
        type="text"
        id="input__also"
        data-value="aqui existe un valor"
      />
      <button onClick={ handleClick }> Click me </button>
    </>
  );
};
