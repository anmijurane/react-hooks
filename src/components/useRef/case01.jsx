import React, { useEffect, useRef, useState } from 'react'

export const ReadWidth = () => {

  const divRef = useRef();
  const [xPosition, setXPosition] = useState(0);

  useEffect(() => {
    const handleScroll = () => {
      const div = divRef.current;
      const { y } = div.getBoundingClientRect();
      setXPosition( y );
    };
    window.addEventListener('scroll', handleScroll);

    return () => { window.addEventListener('scroll', handleScroll) }
  }, [])

  return (
    <div ref={divRef} style={{ width: "100vw", height: "180vh", background: "aqua" }}>
      <h1>{ xPosition }</h1>
    </div>
  );
};
