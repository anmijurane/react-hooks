// este ejemplo muestra el uso del useState en un renderizado condicional
import React, { useState } from 'react';

// rednerizado condicional
export const ConditionalRender = () => {

  // 1.- muestra de valores de renderizado
  // si es verdadero se muestra un mensaje de lo contrario otro mensaje
  const [ isActive, toogleValue ] = useState( false );

  return (
    <div>
      <CheckboxCustom
        label="Correo empresarial"
        name="emailEnterplace"
        value={isActive}
        onChangeCheckbox={ () => toogleValue(!isActive) }
      />
      <p> the checkbox is: {isActive ? 'on' : 'off'} </p>
    </div>
  );
};

const CheckboxCustom = ({ label, name, value, onChangeCheckbox }) => (
  <div className="input__container">
    <label htmlFor={ name }> { label } </label>
    <input
      type="checkbox"
      name={ name }
      id={ name }
      value={ value }
      onChange={ onChangeCheckbox }
    />
  </div>
);
